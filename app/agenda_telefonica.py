#!/usr/bin/env python
# -*- coding: utf-8 -*-
from DataAccess.conexionMySQL import ConexionMySQL
from DataAccess.conexionMongo import ConexionMDB
class AgendaTelefonica:

    def __init__(self):
        self.mdb=ConexionMDB()
        self.sql=ConexionMySQL()

    def listarMongo(self):
        print(self.mdb)

    def anadirMongo(self, nombre , telcelular , telfijo , email , fechanacimiento , direccion):
        mydict = { "nombre": nombre, "telcelular": telcelular, "telfijo": telfijo, "email": email, "fechanacimiento": fechanacimiento, "direccion": direccion}
        x = self.mdb.collection.insert_one(mydict)
        print(x.inserted_id)

    def anadirSQL(self, nombre , telcelular , telfijo , email , fechanacimiento , direccion):
        query=f"INSERT INTO contacto (nombre, telcelular, telfijo, email, fechanacimiento, direccion) VALUES ('{nombre}','{telcelular}', '{telfijo}', '{email}', '{fechanacimiento}','{direccion}')"
        resultado = self.sql.ejecutar_query(query)
        print(resultado)

    def listarSQL(self):
        query="SELECT * FROM contacto"
        result=self.sql.ejecutar_query(query)
        filas=result.fetchall()
        print()
        for fila in filas:
            print(fila[1])
        return True

